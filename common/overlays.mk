PRODUCT_PACKAGES += \
    PixelAccessibilityMenu \
    PixelBuiltInPrintService \
    CaptivePortalLoginOverlay \
    CellBroadcastReceiverOverlay \
    CellBroadcastServiceOverlay \
    PixelContactsProvider \
    GoogleConfigOverlay \
    GoogleDeviceLockControllerOverlay \
    GoogleHealthConnectOverlay \
    GooglePermissionControllerOverlay \
    GoogleWebViewOverlay \
    ManagedProvisioningPixelOverlay \
    PixelConfigOverlay2018 \
    PixelConfigOverlay2019 \
    PixelConfigOverlay2019Midyear \
    PixelConfigOverlay2021 \
    PixelConfigOverlayCommon \
    PixelConnectivityOverlay2023 \
    PixelTetheringOverlay2021 \
    PixelSettingsGoogle \
    PixelSettingsProvider \
    SystemUIGXOverlay \
    PixelSystemUIGoogle \
    PixelTeleService \
    PixelTelecom \
    Pixelframework-res \

# Pixel Launcher
TARGET_INCLUDE_PIXEL_LAUNCHER ?= true
ifeq ($(TARGET_INCLUDE_PIXEL_LAUNCHER),true)
PRODUCT_PACKAGES += \
    PixelLauncherOverlay
endif
